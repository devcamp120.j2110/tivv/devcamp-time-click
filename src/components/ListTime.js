import { Component } from "react";

class ListTime extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            time: [this.getCurrentTime()]
        }
    }
    getCurrentTime = () => {
        var dateTime = new Date();
        var currentTime = dateTime.getHours() + ':' + dateTime.getMinutes() + ':' + dateTime.getSeconds();
        return currentTime;
    }
    onBtnAddClick = () => {
        this.setState({
            time: [...this.state.time , this.getCurrentTime()]
        })
    }
    render() {
        return(
            <div className="container pt-5">
                <button className="btn btn-success" onClick={this.onBtnAddClick}>Add to List</button>
                {this.state.time.map((element, index) => {
                        return <p key={index}>{index + 1} : {element}</p>
                    })}
            </div>
        )
    }
}
export default ListTime;